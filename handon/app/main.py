import os
from UnleashClient import UnleashClient


def main(unleash_client):
    print('Hi LLA Team')

    # Loading environment variables
    DBPASSWORD = os.getenv('MYPROJECT_DBPASSWORD', '')
    print(f'the password is: {DBPASSWORD}')

    # check the feature flag to enable or disable unfinished feature flag.
    if unleash_client.is_enabled('de-20220207-rel-handson'):
        new_feature()
    

# TODO: complete the code for this new feature
def new_feature():
    "This the uncomplete code for a new feature"

    print("uncomplete code flag enabled")



if __name__ == '__main__':
    
    # client for reading project feature flags
    client = UnleashClient(
        url=os.getenv('MYPROJECT_FEATURE_FLAGS_URL', ''),
        app_name=os.getenv('MYPROJECT_ENVIROMENT', ''),
        instance_id=os.getenv('MYPROJECT_FEATURE_FLAGS_TOKEN')
    )
    client.initialize_client()

    main(client)
